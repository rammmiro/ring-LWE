#include <nfl.hpp>
#include <chrono>
#include <iostream>
#include <stdio.h>

#ifndef _N
#define _N 512
#endif

#ifndef _REP
#define _REP 1000
#endif

#ifndef _SEED
#define _SEED 0 // Used to generate random vectors as plain texts
#endif

#ifndef _BITS
#define _BITS 14 
#endif

#if _BITS == 14
#define P_TYPE uint16_t
#elif _BITS == 30
#define P_TYPE uint32_t
#elif _BITS == 62
#define P_TYPE uint64_t
#endif

#ifndef _SIGMA
#if _N == 512
#define _SIGMA 5.6160
#elif _N == 256
#define _SIGMA 6.4812
#else
#define _SIGMA 0
#endif
#endif

/*
#define _N 512
#define _REP 1000
#define _SEED 0
#define _BITS 30
#define P_TYPE uint32_t
#define _SIGMA 5.6160
*/

using poly_type = nfl::poly_from_modulus<P_TYPE, _N, _BITS>;
//using poly_type = nfl::poly_from_modulus<uint16_t, _N, 14>;
//using poly_type = nfl::poly_from_modulus<uint32_t, 256, 30>;
using gauss = nfl::gaussian<uint8_t, typename poly_type::value_type, 2>;
using fast_gauss = nfl::FastGaussianNoise<uint8_t, typename poly_type::value_type, 2>;

struct PublicKey {
    poly_type a_i;
    poly_type b_i;
};

struct SecretKey {
    poly_type s_i;
    poly_type s_prime;
};

struct Encryption {
    poly_type u;
    poly_type v;
};

size_t q = poly_type::get_modulus(0);
size_t q_1_2 = q/2 + 1;
size_t q_1_4 = q/4;
size_t q_3_4 = q/2 + q/4 + 1;

template <class T>
double get_time_us(T const& start, T const& end, uint32_t rep) {
    auto diff = end-start;
    return (long double)(std::chrono::duration_cast<std::chrono::microseconds>(diff).count())/rep;
}

template <class T>
unsigned int count_differences(std::vector<T> const &v1, std::vector<T> const &v2) {
    unsigned int diffs = 0;
    for (unsigned int i = 0; i < v1.size(); i++) {
        if (v1[i] != v2[i]) {
            diffs++;
        }
    }
    return diffs;
}

// ALERT: This only works when coefficients only use one word of the base type
void print_poly(poly_type &p) {
    int i = 0;
    for (auto& it : p) {
        if (i == 0) {
            printf("%d + ", it);
        } else if (i == p.degree - 1) {
            printf("%dx^%d", it, i);
        } else {
            printf("%dx^%d + ", it, i);
        }
        i++;
    }
    printf("\n");
}

void print_coeffs(poly_type &p) {
    for (auto& it : p) {
        printf("%d\n", it);
    }
}

void print_bitvec(std::vector<bool> &vec) {
    printf("[");
    for (int i = 0; i < vec.size(); i++) {
        if (i == vec.size() - 1) {
            printf("%d", (bool) vec[i]);
        } else {
            printf("%d, ", (bool) vec[i]);
        }
    }
    printf("]\n");
}

int poly_from_bitvector(poly_type &p, std::vector<bool> &vec) {
    if (vec.size() != p.degree) {
        fprintf(stderr, "Bit vector size and poly degree differs\n");
        return -1;
    }
    // Since poly coefficients are 0 or 1, we can find them all at cm = 0
    for (size_t i = 0; i < p.degree; i++) {
        p(0, i) = (bool) vec[i];
    }
    return 0;
}

int bitvector_from_poly(std::vector<bool> &vec, poly_type &p) {
    if (vec.size() != p.degree) {
        fprintf(stderr, "Bit vector size and poly degree differs\n");
        return -1;
    }
    for (size_t i = 0; i < p.degree; i++) {
        vec[i] = (bool) p(0, i);
    }
    return 0;
}

void print_poly_test(poly_type &p) {
    printf("===\n");
    for (size_t cm = 0; cm < p.nmoduli; cm++) {
        for (size_t i = 0; i < p.degree; i++) {
            printf("%d ", p(cm, i));
        }
        printf("\n");
    }
    printf("===\n");
}

// TODO: Make this general
void scale_poly_1_q2(poly_type &p) {
    //size_t q2 = p.nmoduli / 2 + 1;
    // let's use a fixed q2 as a POC, valid for q=15361
    //printf("q2 = %lu\n", q2);
    for (auto& it : p) {
        it *= q_1_2;
    }
}

void scale_poly_q2_1(poly_type &p) {
    //size_t q2 = p.nmoduli / 2 + 1;
    // let's use a fixed q2 as a POC, valid for q=15361
    for (auto& it : p) {
        if (it < q_1_4 || it > q_3_4) {
            it = 0;
        } else {
            it = 1;
        }
    }
}

int gen_key_pair(PublicKey &pk, SecretKey &sk, gauss &gauss_s) {
    pk.a_i = poly_type( (nfl::uniform()) );
    sk.s_i = poly_type(gauss_s);
    poly_type e(gauss_s);
    
    pk.a_i.ntt_pow_phi();
    sk.s_i.ntt_pow_phi();
    sk.s_prime = nfl::compute_shoup(sk.s_i);

    pk.b_i = pk.a_i * sk.s_i;
    //pk.b.invntt_pow_invphi();
    //pk.a.invntt_pow_invphi();
    //sk.s.invntt_pow_invphi();
    e.ntt_pow_phi();
    pk.b_i = pk.b_i + e;

    /*
    printf("Key Generation...\n");
    printf("a =\n");
    print_poly(pk.a);
    printf("b =\n");
    print_poly(pk.b);
    printf("s =\n");
    print_poly(sk.s);
    printf("e =\n");
    print_poly(e);
    */

    //print_poly_test(e);

    return 0;
}

int encrypt(PublicKey &pk, std::vector<bool> &msg, Encryption &enc, gauss &gauss_s) {
    poly_type zq2;
    if (poly_from_bitvector(zq2, msg) != 0) {
        return -1;
    }
    scale_poly_1_q2(zq2);

    // We use a copy of the pk because we will be transforming (ntt) it's values 
    // during the process.
    //poly_type a = pk.a;
    //poly_type b = pk.b;

    poly_type r(gauss_s);
    poly_type e1(gauss_s);
    poly_type e2(gauss_s);

    //print_coeffs(r);

    //a.ntt_pow_phi();
    //b.ntt_pow_phi();
    r.ntt_pow_phi();
    enc.u = pk.a_i * r;
    enc.v = pk.b_i * r;
    enc.u.invntt_pow_invphi();
    enc.v.invntt_pow_invphi();
    // No need to invntt a and b because they are local copies
    enc.u = enc.u + e1;
    enc.v = enc.v + e2 + zq2;

    /*
    printf("Encryption...\n");
    printf("r =\n");
    print_poly(r);
    printf("e1 =\n");
    print_poly(e1);
    printf("e2 =\n");
    print_poly(e2);
    printf("u =\n");
    print_poly(enc.u);
    printf("v =\n");
    print_poly(enc.v);
    printf("zq2 =\n");
    print_poly(zq2);
    */
    
    /*
    // BEGIN TESTING
    poly_type u = enc.u;
    //poly_type s = sk.s;
    poly_type zq2_;

    u.ntt_pow_phi();
    //s.ntt_pow_phi();
    zq2_ = nfl::shoup(u * sk.s_i, sk.s_prime);
    zq2_.invntt_pow_invphi();
    zq2_ = enc.v - zq2_ - zq2;
    //print_coeffs(zq2_);
    // END OF TESTING
    */
    
    return 0;
}

int decrypt(SecretKey &sk, std::vector<bool> &msg, Encryption &enc) {
    // We use a copy of u and s because we will be transforming (ntt) it's values 
    // during the process.
    poly_type u = enc.u;
    //poly_type s = sk.s;
    poly_type zq2;

    u.ntt_pow_phi();
    //s.ntt_pow_phi();
    zq2 = nfl::shoup(u * sk.s_i, sk.s_prime);
    zq2.invntt_pow_invphi();
    zq2 = enc.v - zq2;

    /*
    printf("Decryption...\n");
    printf("zq2 =\n");
    print_poly(zq2);
    */

    scale_poly_q2_1(zq2);
    //printf("z =\n");
    //print_poly(zq2);

    if (bitvector_from_poly(msg, zq2) != 0) {
        return -1;
    }
    return 0;
}

int main() {
    // sigma = s / sqrt(2*pi)
    double sigma = _SIGMA; // The standard deviation of the gaussian.
    std::cerr << "Using n=" << _N << ", q=" << q << ", sigma=" << sigma << std::endl;
    // Define the gaussian generator object
    fast_gauss g_prng(sigma, 128, 1<<15, (double)0, false);
    // Define a struct to access the generator via a constructor
    gauss gauss_s(&g_prng, 1);

    auto start = std::chrono::steady_clock::now();
    auto end = std::chrono::steady_clock::now();

    PublicKey *pk[_REP];
    SecretKey *sk[_REP];

    start = std::chrono::steady_clock::now();
    for (int i = 0; i < _REP; i++) {
        pk[i] = (PublicKey*) malloc(sizeof(PublicKey));
        sk[i] = (SecretKey*) malloc(sizeof(SecretKey));
        gen_key_pair(*pk[i], *sk[i], gauss_s);
    }
    end = std::chrono::steady_clock::now();

    auto gen_time = get_time_us(start, end, _REP);
    std::cerr << "Time to generate key pair: " 
        << gen_time << " us" << std::endl; 

    Encryption *msg_enc[_REP];

    // generate random vectors
    std::mt19937 mersenne_engine(_SEED);
    std::uniform_int_distribution<int> dist(0, 1);
    auto gen = std::bind(dist, mersenne_engine);

    std::vector<bool> msg_plain[_REP];
    for (int i = 0; i < _REP; i++) {
        msg_plain[i].resize(_N);
        generate(std::begin(msg_plain[i]), std::end(msg_plain[i]), gen);
    }

    start = std::chrono::steady_clock::now();
    for (int i = 0; i < _REP; i++) {
        msg_enc[i] = (Encryption*) malloc(sizeof(Encryption));
        encrypt(*pk[i], msg_plain[i], *msg_enc[i], gauss_s);
    }
    end = std::chrono::steady_clock::now();

    auto enc_time = get_time_us(start, end, _REP);
    std::cerr << "Time to encrypt: " 
        << enc_time << " us" << std::endl; 

    std::vector<bool> msg_dec[_REP];
    for (int i = 0; i < _REP; i++) {
        msg_dec[i].resize(_N);
    }

    start = std::chrono::steady_clock::now();
    for (int i = 0; i < _REP; i++) {
        decrypt(*sk[i], msg_dec[i], *msg_enc[i]);
    }
    end = std::chrono::steady_clock::now();

    auto dec_time = get_time_us(start, end, _REP);
    std::cerr << "Time to decrypt: " 
        << dec_time << " us" << std::endl; 

    // Check for decoding errors:
    long err_cnt = 0;
    long bit_cnt = 0;
    long bit = 0;
    for (int i = 0; i < _REP; i++) {
        bit = count_differences(msg_plain[i], msg_dec[i]); 
        if (bit > 0) {
            err_cnt++;
        }
        bit_cnt += bit;
    }
    std::cerr << "Decoding errors: " 
        << err_cnt << "/" << _REP << " (" << (double)err_cnt/_REP <<") messages, "
        << bit_cnt << "/" << _REP*_N <<  " (" << (double)bit_cnt/(_REP*_N) <<") bits" 
        << std::endl; 

    std::cerr << "Public key size: " << _N*_BITS*2 << " bits "<< std::endl; 
    std::cerr << "Private key size: " << _N*_BITS << " bits "<< std::endl; 
    std::cerr << "Encryption blow up factor: x" << _BITS*2 << std::endl; 
    //print_bitvec(msg);
    //print_bitvec(msg1);

    printf(" & %d $\\mu s$ & %d $\\mu s$ & %d $\\mu s$ & %.4f\\\% & %.4f\\\% & %.1f kb & %.1f kb \\\\\n",
            (int)gen_time, (int)enc_time, (int)dec_time,
            (double)err_cnt*100/_REP, (double)bit_cnt*100/(_REP*_N),
            (double)(_N*_BITS*2)/1024.0, (double)_N*_BITS/1024.0);


    for (int i = 0; i < _REP; i++) {
        free(pk[i]);
        free(sk[i]);
        free(msg_enc[i]);
    }

    return 0;
}
