LIB_NFL=/media/data0/Documents/Universitat/MIRI/Q4/TFM/implementations/NFLlib/_build/nfllib/lib
INC_NFL=/media/data0/Documents/Universitat/MIRI/Q4/TFM/implementations/NFLlib/_build/nfllib/include

LDLIBS=$(LIB_NFL)/libnfllib_static.a -lmpfr -lgmp
#LDLIBS=-rdynamic libnfllib.so


CXX=g++
#CXX=clang++
CXXFLAGS=-c -std=c++11
CPPFLAGS=-I$(INC_NFL) -I$(INC_NFL)/prng -I$(INC_NFL)/arch
#CPPFLAGS=-c -g -Wall

debug: CXXFLAGS += -g
debug: all

all: ringlwe

ringlwe: ringlwe.o
	$(CXX) ringlwe.o -o ringlwe $(LDLIBS)

ringlwe.o: ringlwe.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) ${PARAMS} ringlwe.cpp

clean:
	rm -f *.o ringlwe
