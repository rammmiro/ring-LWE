# Ring-LWE public-key encryption

This project contains a Ring-LWE public-key encryption scheme implemented as
described in [LP13] using the NFLLlib library [NFL].

To compile the code first obtain a copy of the NFLlib from [NFL] and build the
library.  Then, edit the first two lines of the Makefile to point to the folder
where the library was built and to point to the headers of the library.

After that run
```
make
```

To run the benchmarks, simply do
```
./run.sh
```

### References

[LP13] Vadim Lyubashevsky, Chris Peikert and Oded Regev - "On Ideal Lattices and
Learning with Errors over Rings"
[NFL] Carlos Aguilar, Tancrède Lepoint, Adrien Guinet and Serge Guelton <https://github.com/quarkslab/NFLlib>
