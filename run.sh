#! /bin/sh

#set -x

PI=3.141592653589793

benchmark() {
	SIGMA=$(echo "$S/sqrt(2*$PI)" | bc -l)
	echo "\tN=$N, BITS=$BITS, S=$S"
	make -s clean
	make -s PARAMS="-D_N=$N -D_BITS=$BITS -D_SIGMA=$SIGMA -D_REP=$REP"
	echo "Running..."
	./ringlwe
}

REP=50000

N=256
BITS=14
S=16.555496
benchmark

N=256
BITS=30
S=4376.414009
benchmark

N=512
BITS=14
S=13.921417
benchmark

N=512
BITS=30
S=3680.238720
benchmark

N=256
BITS=14
S=14.764801
benchmark

N=256
BITS=30
S=3903.110179
benchmark

N=512
BITS=14
S=12.415588
benchmark

N=512
BITS=30
S=3282.179041
benchmark

#echo "\t< 128"
#N=256
#BITS=14
#S="16.2460"
#benchmark


#echo "\t> 128"
#N=256
#BITS=14
#S="32"
#benchmark

#echo "\t> 256"
#N=256
#BITS=30
#S="1048320"
#benchmark

#echo "\t> 256"
#N=512
#BITS=14
#S="45.2548"
#benchmark

#echo "\t> 128"
#N=512
#BITS=30
#S="3721.47"
#benchmark

#echo "\t> 128"
#N=512
#BITS=30
#S="3400.43"
#benchmark


#echo "\t> 256"
#N=512
#BITS=14
#S="26"
#benchmark
#
#echo "\t> 128"
#N=512
#BITS=14
#S="2"
#benchmark
#
#echo "\t> 128"
#N=512
#BITS=30
#S="700"
#benchmark
#
#echo "\t> 256"
#N=512
#BITS=30
#S="34500"
#benchmark
#
#echo "\t> 128"
#N=256
#BITS=30
#S="55000"
#benchmark
